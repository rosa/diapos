# Diapositivas de TeknoKasa

Accesible en [https://rosa.frama.io/diapos](https://rosa.frama.io/diapos)

## Consejos Reveal.js

Estas son algunas características y atajos Reveal.js útiles.

- 's' - tipo 's' para entrar en modo ponente, que abre una ventana separada con un tiempo y notas del ponente
- 'o' - tipo 'o' para introducir modo de visión general y desplazarse a través de las miniaturas de diapositivas
- 'f' - tipo 'f' para entrar en modo de pantalla completa

## Licencia

Código con AGPLv3 y contenido con CC BY-NC-SA 4.0