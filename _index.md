+++
title = "Taller de webs estáticas"
outputs = ["Reveal"]
[reveal_hugo]
theme = "night"
margin = 0.2
[logo]
src = "../logos/teknokasa/teknokasa_logo_full.png"
+++

{{% section %}}

# Taller de webs estáticas

---

Aprenderemos a crear y editar webs estáticas 

---

Webs estáticas generadas con Hugo y alojadas en GitLab Pages

---

Siguiendo diferentes guías:

Edición de una web existente
- Desde un navegador 
- En local usando git

Creando una nueva web

---

{{< slide background-image="../backgrounds/together_we_create.jpg" >}}
{{% /section %}}

---
{{% section %}}

# Teoría general
¿Qué es una web? ¿Web dinámica vs web estática?

(Añadir memes para que sea ameno e inspirador)

{{% note %}}
- Se pueden **añadir notas** y verlas pulsando la letra s 
{{% /note %}}

---
## ¿Qué es una web?
(Arquitectura Cliente/servidor, HTML y CSS, ...)

---

## Web estática VS Dinámica
(Usabilidad, seguridad, mantenimiento, ...)
{{% /section %}}


---

{{% section %}}
# Teoría específica

Hugo, Markdown, Git y Gitlab Pages

---

## Hugo

(...)

---

## Markdown

(...)

---

## Git

(...)

---

## GitLab Pages

(...)

---

{{% /section %}}

---

{{% section %}}
# Práctica

Edición de una web existente

---

- Mediante la interfaz gráfica de GitLab
- En local con línea de comandos

---

Requisitos:

- Cuenta en la instancia de GitLab donde se aloja la página web
- Conocimientos básicos del lenguaje de marcado Markdown

---

[Guías para editar una web](https://rosa.frama.io/teknokasa/docs/taller_web/editar_web/)

- [Editar un contenido o entrada ya creada](https://rosa.frama.io/teknokasa/docs/taller_web/editar/)

- [Crear una nueva web](https://rosa.frama.io/teknokasa/docs/taller_web/crear/)

{{% /section %}}

---

{{% section %}}

# Práctica

Generación de una nueva web

---

[Generación de una página web con Hugo](https://hacklab.frama.io/grupos/pagina_web/hugo/generacion_pagina_con_hugo/index.html)

---

[Despliegue en Gitlab pages](https://hacklab.frama.io/grupos/pagina_web/hugo/despliegue_en_gitlab/index.html)

{{% /section %}}