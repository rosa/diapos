+++
title = "Taller de webs estáticas"
outputs = ["Reveal"]
[reveal_hugo]
theme = "night"
margin = 0.2
[logo]
src = "../teknokasa/teknokasa_logo_full.png"
+++

{{% section %}}

# Taller de edición y creación de webs estáticas con Hugo

{{% note %}}
- Bienvenida y presentación del taller
- Ronda de presentación: qué tal nos encontramos y qué nos motiva del taller
- También se puede animar a les participantes a que se asignen varios papeles con diferentes funciones, como moderar, tomar notas, cronometrar, presentar o ejercer de artista (si se requiere una presentación visual)
- Los recursos están disponibles en esta web. Es la primera edición del taller y seguro que hay cosas que se puedan mejorar. 
- La intención de este taller es que sea reproducible y que las personas que haya recibido el taller lo puedan reproducir o mejorar 
{{% /note %}}

---

[rosa.frama.io/diapos/taller_web](https://rosa.frama.io/diapos/taller_web)

[rosa.frama.io/teknokasa](https://rosa.frama.io/teknokasa)

---

Aprender a crear y editar webs estáticas generadas con Hugo y alojadas en GitLab Pages
{{% note %}}
- Objetivo
{{% /note %}}

---

# Índice

---

## Teoría general
 
- ¿Cómo funciona Internet?
- ¿Qué es el desarrollo web?
- Web estática VS Dinámica

{{% note %}}
- Preguntar quién sabría responder a estas preguntas
{{% /note %}}

---

## Teoría específica

- Hugo
- Markdown
- Git y GitLab

{{% note %}}
- Preguntar quién ha utilizado estas tecnologías
{{% /note %}}

---

## Guías

**Editando** una **web ya creada**
- Desde un navegador usando un:
  - gestor de contenido
  - editor de código con interfaz web
- En local usando git

{{% note %}}
Preguntar:
  - ¿Quién sabe lo que es un gestor de contenido?
  - ¿Quién ha usado git/GitHub/GitLab?
{{% /note %}}

---

## Guías

**Creando** una **nueva web estática**
- Generación de una página web con Hugo
- Despliegue con Gitlab Pages
- Añadir un gestor de contenido a tu web estática

{{% note %}}
Preguntar:
  - ¿Quién ha hecho alguna web estática?
  - ¿Quién ha usado Hugo previamente?
{{% /note %}}

---

{{< slide background-image="../backgrounds/2023-11-13_banner_by-David-Revoy.jpg" >}}
{{% /section %}}

---
{{% section %}}

# Teoría general

{{% note %}}
Comentar:
- Esta parte es para sentar una base para entender la práctica, no es necesario conocer todo al detalle para editar o crear una web. Es un proceso de aprendizaje y no todo se aprende en un día
- si hay gente que quiera saltarse la teoría e ir directamente a pegarse con la guías, adelante
- si me columpio en algo, corregidme, aquí estamos todo el mundo aprendiendo
{{% /note %}}

---

## ¿Cómo funciona Internet?

{{% note %}}
Preguntar sobre:
- si algune sabe definirlo
- componentes necesarios para que funciones 
{{% /note %}}

---

{{< slide background-image="../backgrounds/como_funciona_internet_logout_con_cita.png"  >}}

---

{{< slide background-image="../resources/es_howtheinternetworks.png" background-size="contain" href="https://myshadow.org/ckeditor_assets/attachments/264/es_howtheinternetworks.pdf">}}

---

## ¿Qué es el desarrollo web?

{{% note %}}
Preguntar:
- algune sabe definirlo?
- en qué se basa arquitectura cliente/servidor?
- qué lenguaje hablan entre el cliente y el servidor para ver una web? (sin contar protocolos de comunicación)

Comentar por encima: 
- Arquitectura Cliente/servidor es un modelo de diseño de software en el que las tareas se reparten entre los proveedores de recursos o servicios, llamados servidores, y los demandantes, llamados clientes.
- Apoyarse con el diagrama de la diapositiva anterior
- HTML (lenguaje de marcado de hipertexto), CSS (hojas de estilo en cascada) y javascript (acciones)
{{% /note %}}

---

{{< slide background-image="../robadas/do-html-css-web-developing-and-editing.webp" background-size="contain" >}}

{{% note %}}
Remarcar comillas para afirmar que es el "lenguaje que hablan entre el cliente y el servidor para ver una web (sin contar protocolos de comunicación)"
{{% /note %}}

---

```html{}
<!DOCTYPE html>
<html>
    <head>
        <title>Mi página web</title>
    </head>
    <body>
        <h1>Hola , mundo!</h1>
        <p>Es mi primera web.</p>
        <p>Contiene un
             <strong>título principal</strong> y un <em> párrafo </em>.
        </p>
    </body>
</html>
```

![](../ejemplos/html.png)

{{% note %}}
Ejemplo del lenguaje de marcado HTML básico

{{% /note %}}

---

```css{}
body {
  background-color: lightblue;
}
title{
  text-align: center;
}
p {
  text-align: center;
  color: red;
} 
```
![](../ejemplos/css.png)

{{% note %}}
Ejemplo de una hoja de estilo css

{{% /note %}}

---

<section data-noprocess>
  <style>

    * {
    	margin: 0 auto;
    	padding: 0;
    	box-sizing: border-box;
    	font-family: "Poppins", sans-serif;
    }

    body {
    	display: flex;
    	background: #000;
    	min-height: 100vh;
    	align-items: center;
    	justify-content: center;
    }

    .content {
    	position: relative;
    }

    .content h2 {
    	color: #fff;
    	font-size: 8em;
    	position: absolute;
    	transform: translate(-50%, -50%);
      top: 50%;
      left: 50%;
    }

    .content h2:nth-child(1) {
    	color: transparent;
    	-webkit-text-stroke: 2px #8338ec;
    }

    .content h2:nth-child(2) {
    	color: #c19bf5;
    	animation: animate 4s ease-in-out infinite;
    }

    @keyframes animate {
    	0%,
    	100% {
    		clip-path: polygon(
    			0% 45%,
    			16% 44%,
    			33% 50%,
    			54% 60%,
    			70% 61%,
    			84% 59%,
    			100% 52%,
    			100% 100%,
    			0% 100%
    		);
    	}

    	50% {
    		clip-path: polygon(
    			0% 60%,
    			15% 65%,
    			34% 66%,
    			51% 62%,
    			67% 50%,
    			84% 45%,
    			100% 46%,
    			100% 100%,
    			0% 100%
    		);
    	}
    }
  </style>
	<div class="content">
		<h2>ACAB</h2>
		<h2>ACAB</h2>
	</div>

{{% note %}}
Se puede hacer auténtica magia y animaciones increibles como estas

{{% /note %}}

---

```javascript{}
  <button onclick="document.getElementById('bombilla').src='../gifs/pic_bulbon.gif'">Enciende la luz</button>

  <img id="bombilla" src="../gifs/pic_bulboff.gif" style="width:100px">

  <button onclick="document.getElementById('bombilla').src='../gifs/pic_bulboff.gif'">Apaga la luz</button>
```

---
<section data-noprocess>
  <!DOCTYPE html>
  <html>
  <body>

  <h2>Qué se puede hacer con JavaScript?</h2>

  <p>JavaScript puede cambiar atributos del HTML</p>

  <p>En este caso, JavaScript cambia el valor del atributo src (fuente) de una imagen.</p>

  <button onclick="document.getElementById('bombilla').src='../gifs/pic_bulbon.gif'">Enciende la luz</button>

  <img id="bombilla" src="../gifs/pic_bulboff.gif" style="width:100px">

  <button onclick="document.getElementById('bombilla').src='../gifs/pic_bulboff.gif'">Apaga la luz</button>

  </body>
  </html>

---

{{< slide background-image="../robadas/servidor_aplicaciones.png" background-size="contain" >}}

{{% note %}}
Recogiendo lo anterior y como resumen básico de componentes necesarios hasta ahora:
- La arquitectura cliente-servidor sigue siendo parte fundamental hoy en día de Internet.
- Un cliente == navegador web
- Los **servidores web y los servidores de aplicaciones** son las tecnologías que **permiten el intercambio de datos y servicios** a través de Internet. Cuando visita un sitio web o una aplicación, su navegador (como cliente) solicita datos de un servidor remoto y muestra la respuesta.
  - **Servidor web**
    - es una tecnología que aloja el código y los datos de un sitio web
    - los servidores web ofrecen contenido estático, como texto, imágenes, videos y archivos. Los servidores de aplicaciones ofrecen contenido dinámico, como actualizaciones en tiempo real, información personalizada y atención al cliente. 
  - **Servidor de aplicaciones**
    - amplía las capacidades de un servidor web, pues admite la generación de contenido dinámico, la lógica de la aplicación y la integración con varios recursos
    - Proporciona y ejecuta programas. Estos incluyen servicios como autenticación, transacciones, directorios o bases de datos
- Comentar que se ha desdibujado la línea entre los servidores web y los servidores de aplicaciones, sobre todo porque el navegador web ha surgido como el cliente de aplicaciones preferido

{{% /note %}}

---

## Web estática VS Dinámica

{{% note %}}
Preguntar:
  - algune sabe la diferencia?

Una **web estática no cambia el contenido** y las **páginas dinámicas pueden mostrar contenido diferente** a **diferentes usuaries**

Una **página web dinámica** es un tipo de sitio web que **genera contenido en tiempo real** _en función_ de la **interacción del usuarie**. A diferencia de las páginas web estáticas, las páginas web dinámicas pueden personalizarse y cambiar en tiempo real.
{{% /note %}}

---

## Web estáticas

#### Ventajas

- Más ligeras
- Más seguras
- Infraestructura más simple y mejor rendimiento

#### Desventajas

- Sin contenido personalizado a distintes usuaries
- No ideales para webs con muchas actualizaciones o muchos datos


{{% note %}}
Ventajas:
- **Más ligeras**: minimizan la huella de carbono y bajo consumo eléctrico
- **Más seguras**: no interactúan con bases de datos ni procesan información del usuarie
- Infraestructura más **simple** y **mejor rendimiento**

Desventajas:
- No ofrecen contenido personalizado a distintes usuaries
- No convenientes a para webs que necesitan de actualizaciones constantes o con grandes volúmenes de datos

{{% /note %}}

---

## Web dinámicas

#### Ventajas

- Experiencias personalizadas y únicas
- Posibilidad de actualizar y cambiar el contenido automáticamente

#### Desventajas

- Infraestructura más compleja
- Costoso mantenimiento
- Más vulnerables

{{% note %}}

Ventajas:

- Ofrecen contenido personalizado y experiencias de usuarie únicas.
- Capacidad para actualizar y cambiar el contenido automáticamente.

Desventajas de las páginas web dinámicas:

- Requieren de habilidades de programación más avanzadas y una infraestructura más compleja.
- Pueden ser más costosas de mantener debido a su complejidad y a la necesidad de actualizaciones constantes.
- Mayor riesgo de vulnerabilidades de seguridad debido a la interacción con bases de datos y procesamiento de información del usuarie.

{{% /note %}}

---

{{< slide background-video="../gifs/cat-wait.mp4" background-size="contain" >}}


{{% /section %}}

---

{{% section %}}

# Teoría específica

{{% note %}}
Hugo, Markdown, Git y Gitlab Pages
{{% /note %}}

---

## Hugo

Es un **generador de páginas estáticas** escrito en Go

{{% note %}}
Importante: **no requiere saber Go**, sólo markdown, git y usar una terminal. Para sacarle el máximo partido está bien aprender lo básico de Go, Git, FrontEnd (HTML, CSS y JS) y Markdown. Siempre es buen momento de empezar a aprender.
{{% /note %}}

---

![](https://www.ionos.mx/digitalguide/fileadmin/DigitalGuide/Screenshots_2022/Static-site-generator-diagrama-de-funcionamiento.png)

---

## Markdown

Es un lenguaje de marcado de texto y se usa como una **forma sencilla de agregar formato a textos web**

---

![](https://i0.wp.com/www.silocreativo.com/wp-content/uploads/2019/05/markdown-cheatsheet.png?resize=1199%2C704&quality=100&strip=all&ssl=1)

---

## Git

Git es un **sistema de control de versiones** distribuido, rápido y escalable que permite **gestionar y rastrear cambios en el código fuente** de un proyecto

---

![](https://desarrollowp.com/wp-content/uploads/2020/10/Git-Working-Tree.png)


---

{{< slide background-image="../robadas/Comandos_De_Git-1024x1024.jpg" background-size="contain" >}}

---

## GitLab 

GitLab es una **plataforma de gestión de proyectos y control de versiones** que utiliza el sistema de control de versiones Git

---

## Instancias de GitLab

**Framagit**.org es una instancia mantenida por Framasoft

**0xacab**.org es una instancia mantenida por Riseup

Gitlab.com es la instancia mantenida por Gitlab Inc


{{% note %}}

**Framasoft** es una asociación francesa sin ánimo de lucro dedicada a la promoción del software libre, la cultura libre y los servicios éticos en línea

**Riseup** es un colectivo que proporciona herramientas de comunicación online para gente y grupos que trabajan en un cambio social libertario

**GitLab** es una compañía de núcleo abierto y la principal proveedora del software GitLab

> El modelo de núcleo abierto (open-core model) es un modelo de negocio para la monetización de software de código abierto producido comercialmente. **Implica** principalmente **ofrecer una versión "básica" o con funciones limitadas** de un **producto de software** como **software gratuito y de código abierto**, mientras que ofrece **versiones "comerciales" o complementos** como **software propietario**. Wikipedia, Modelo de núbleo abierto: https://es.wikipedia.org/wiki/Modelo_de_n%C3%BAcleo_abierto

{{% /note %}}

---

## GitLab CI / CD

Es herramienta que implementa las metodologías de integración, entrega e implementación continua con el objetivo de realizar actividades como el desarrollo, prueba y publicación de softwares

---

{{< slide background-image="../robadas/gitlab-ci.jpg" background-size="contain" >}}

---

## GitLab Pages

Es una **funcionalidad** del software de GitLab que **permite el alojamiento de páginas estáticas *gratuitamente y fácil de configurar*** y ***fácil de configurar***.

Se encarga de compilar el código y publicar el resultado de páginas estáticas

---

{{< slide background-image="../robadas/gitlab-hugo-ci.png" background-size="contain" >}}

---

{{< slide background-video="../gifs/perro-tortuga.mp4" background-size="contain" >}}

{{% /section %}}

---

{{% section %}}

# Práctica I

Edición de una web con Decap CMS / Sveltia CMS

{{% note %}}
Contras:
- En netlify sólo se puede invitar a 5 personas.

Pros:
- Es open source, se puede alojar y no depender de Netlify

- Por qué un CMS basado en Git podría ser el futuro de la gestión de contenidos
  - Utilizar la potencia del versionado y el historial de Git. No necesitad de copias de seguridad
  - Trabajar en el mismo sistema de ramas y entornos que su código.
  - No necesita base de datos ni servidor: utilizar el propio proveedor de Git.
{{% /note %}}


{{% /section %}}

---

{{% section %}}

# Práctica II

Edición de una web existente

---

- Mediante la interfaz gráfica de GitLab
- En local con línea de comandos

---

Requisitos:

- Cuenta en la instancia de GitLab donde se aloja la página web
- Conocimientos básicos de la [sintaxis de Markdown](https://markdown.es/sintaxis-markdown/) y la [estructura de directorios de Hugo](https://gohugo.io/getting-started/directory-structure/)

---

[Guías para editar una web](https://rosa.frama.io/teknokasa/docs/taller_web/editar_web/)

- [Editar un contenido o entrada ya creada](https://rosa.frama.io/teknokasa/docs/taller_web/editar/)

- [Crear una nueva web](https://rosa.frama.io/teknokasa/docs/taller_web/crear/)

{{% /section %}}

---

{{% section %}}

# Práctica III

Generación de una nueva web

---

[Guía de cómo crear una página web con Hugo](https://rosa.frama.io/teknokasa/docs/taller_web/crear/)

- [Generación de una página web con Hugo](https://rosa.frama.io/teknokasa/docs/taller_web/crear/generacion_pagina_con_hugo/)

- [Despliegue en Gitlab pages](https://hacklab.frama.io/grupos/pagina_web/hugo/despliegue_en_gitlab/index.html)

- [Añadir un gestor de contenido](https://rosa.frama.io/teknokasa/docs/taller_web/crear/a%C3%B1adir_cms/)

{{% /section %}}