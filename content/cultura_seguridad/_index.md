+++
title = "Cultura de la seguridad"
outputs = ["Reveal"]
[reveal_hugo]
theme = "night"
margin = 0.2
[logo]
src = "../logos/teknokasa/teknokasa_logo_full.png"
+++

{{% section %}}

# Cultura de la seguridad
{{% note %}}
- Bienvenida y presentación de ponentes (5 min)

- Presentación del taller (5 mins)
  - Motivación: Ante la situación represiva que vivimos actualmente, solemos responder desde los colectivos que "me cuidan mis amigues, no la policía", pero ¿sabemos cuidarnos ante la represión y otras amenazas? ¿qué practicas y hábitos tenemos en nuestro activismo que nos exponen insconscientemente a nosotres y al resto de compas?

  - Surge la necesidad de pensar y promover seguridad y confianza en grupos a través de grupos de afinidad.
  
  - El resultado es este taller inmersivo o participativo. 

  - Notas:
  	- Se agradecen críticas y mejoras. Es la primera edición del taller y seguro que hay cosas que se puedan mejorar. 
  	- Puntos generales a destacar:
      - **No hay preguntas tontas**
      - **No juzgar las prácticas de les demás**
      - **No damos consentimiento a sacar fotos o grabar audio y mucho menos a difundirlo. Si queréis hacer alguna foto, avisadnos previamente**

- Ronda de presentación (10 mins): qué tal nos encontramos y qué nos motiva del taller
  - Si hay mucha gente hacer grupos de 4-5 personas para que todo el mundo pueda hablar y empezar a generar confianza

- La intención de este taller es que sea reproducible y que las personas que haya recibido el taller lo puedan reproducir o mejorar 
{{% /note %}}

---

[rosa.frama.io/diapos/cultura_seguridad](https://rosa.frama.io/diapos/cultura_seguridad)

---

¿Por que es importante la seguridad en el colectivo?

¿Que información es necesaria que sepa? ¿Qué información puedo no tener?

¿Confío en nuestras compañeres?

{{% note %}}
- Objetivos: 
  - Repensar estas preguntas introductorias
  - Plantar varias semillas y regar el arbol de la curiosidad para que florezcan prácticas de confianza y seguridad
  - Cultura de la seguridad de forma transversal en todas las luchas
{{% /note %}}

{{% /section %}}

---
{{% section %}}

# Confianza

---

## Dinámica 1

{{% note %}}
- Juego del secuestro (15 mins)
{{% /note %}}

---

## Dinámica 2

{{% note %}}
- ¿Qué es la seguridad? ¿Qué es la cultura de la seguridad? Análisis de riesgos en contexto de un CSOA (20 mins) 19:05
  
  - Intentar hacer análisis de daños y riesgos concretos (analógicos y digitales)

  - Dinámica en papel con dos coordenadas
    - Qué es más probable (eje x)
    - Qué es más critico (eje y)
{{% /note %}}


{{% /section %}}

---
{{% section %}}

# Autodefensa digital

---

# Índice

---

**Conceptos previos**
+ Contexto
+ Seguridad poco a poco
+ Los malos a.k.a modelo de amenazas
+ Seguridad operacional vs. Seguridad intrumental
+ Primeros pasos

---

**Estructura**
+ Problema ![warning.png](../iconos/warning_1686942075028_0.png)
+ Preguntas ![question.png](../iconos/question_1686942044135_0.png)
+ Soluciones ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png)

--- 

**Primeros pasos en autodefensa digital**
+ Contraseñas
+ Software
+ Dispositivos
+ Bloqueo de móvil
+ Cifrado de ordenador
+ Conversaciones

--- 

**Privacidad y Anonimato**
+ Recolección masiva
+ Privacidad
+ Redes
+ Reconocimiento físico

---

**Seguridad Avanzada**
+ Separación de perfiles
+ No crear más información de la necesaria
+ Pensamiento ofensivo
+ OSINT
+ Ingeniería social

---

**Referencias**


{{% /section %}}

---

{{% section %}}


# Conceptos previos

---

## La tecnología es política

{{% note %}}

Empezando con el contexto: existe una lucha por el control de la tecnología (por conocerla y ser capaz de adaptarla). En dicho proceso hay una lucha por el control y mediación tecnológica del mundo, por lo que controlar la tecnología es controlar el mundo.

{{% /note %}}

---

{{< slide background-image="https://telegra.ph/file/f510273db71478f2dc050.jpg" background-size="contain" >}}

{{% note %}}

Vivimos en una sociedad capitalista y la sociedad nos organiza de forma capitalista, respecto a la tecnología y en Internet. Si queremos superar eso, parte de la filosofía hacker es crear espacios independientes del capital en la red. Redes descentralizadas, software libre, que den independencia a la clase trabajadora como sujeto y cree sobre estos proyectos.

La tecnología va hacia donde quiera que vaya el que la sustenta. En este caso, el dinero, el capital y finalmente los monopolios.

{{% /note %}}

---

## La información es poder

![imagen-valor-datos.jpg](../robadas/imagen-valor-datos_1686951162205_0.jpg)

{{% note %}}

Tener infraestructura propia para la gestión de nuestros datos es político porque el sistema está pensado para el extractivismo de datos

{{% /note %}}


---

## ¿Qué hacemos con la vigilancia?

{{% note %}}

- Camaras con IA detectan intencionalidad de delito

{{% /note %}}

---

## Capitalismo de Vigilancia
![image.png](../robadas/image_1686190705183_0.png)

---

{{< slide background-image="https://www.alejandrobarros.com/wp-content/uploads/2021/02/discovery-of-behavioral-surplus.png" background-size="contain" >}}

{{% note %}}

- Modelos predictivos de publicidad y comportamiento futuro 
- Uso de datos de comportamiento para modelos predictivos de publicidad

{{% /note %}}

---

# Escándalo

![https://i0.wp.com/kokomansion.com/wp-content/uploads/2018/04/Cambridge-Analytica.jpg?fit=700%2C428&ssl=1](https://i0.wp.com/kokomansion.com/wp-content/uploads/2018/04/Cambridge-Analytica.jpg?fit=700%2C428&ssl=1)

---

# 2013

- Publicaciones de Snowden 
- Programas PRISM, XKEYSCORE o TEMPORA

---

![CIA.jpg](../memes/CIA_1687016701674_0.jpg)

---

# Chat Control

{{% note %}}

Iniciativa legislativa europea que propone las empresas tienen que vigilar el contenido que hay dentro de los chats en búsqueda de pedofilia. Para implementarlo hay que eliminar el cifrado e2e, viola el articulo 7 de la carta magna europea. En mayo se votó en contra, pero sigue dando tumbos. Se está escribiendo y renegociando.

España es uno de los más radicales. Fernando Grande-Marlaska, ministro de Interior español, es uno de los grandes defensores de esta iniciativa en Europa. En un documento filtrado en 2023, el ministro exponía que es "imperativo que tengamos acceso a los datos" y "es igualmente imperativo que tengamos la capacidad de analizarlos, sin importar cuán grande sea el volumen".

{{% /note %}}

--- 

# Efecto panóptico

{{% note %}}

Sentir que nuestros actos estén bajo inspección, se rechazan pensamientos o actos subversivos. Revolución en el comportamiento humano para que no se cuestionen ciertos paradigmas establecidos por convención social.

{{% /note %}}

![efecto panóptico](https://i.pinimg.com/originals/09/9d/72/099d7246092b0171bf6a1f2ba7db9664.png)

---

# Violencias digitales

{{% note %}}

La ciberviolencia es toda acción dolosa, llevada a cabo mediante el uso de tecnologías de la información y la comunicación (TIC), por la que se exponga, distribuya, difunda, exhiba, transmita, comercialice, oferte, intercambie o comparta imágenes, audios o vídeos reales o simulados de contenido íntimo sexual de una persona sin su consentimiento, aprobación y autorización. Y que le cause daño psicológico o emocional en cualquier ámbito de su vida privada o en su imagen propia.

1. **Ciberacoso o ‘cyberbullying’**.

En algunos países, el cyberbullying es un delito y se basa en la difusión de imágenes sin el consentimiento de la persona acosada, mensajes hirientes, amenazas y suplantación de identidad. Dentro de su oferta de contenidos didácticos, Segurilatam ofrece unos consejos para prevenir el ciberacoso.

2. **Creación de perfiles falsos**.

Otra forma de ciberviolencia es la originada por la creación de perfiles falsos por parte del acosador, quien, sirviéndose del nombre real de la víctima, procede a publicar detalles íntimos. De esta forma, la persona afectada pasa a encontrarse en una situación de gran vulnerabilidad.

3. **‘Cyberstalking’**.

Esta modalidad de violencia digital se caracteriza por el seguimiento e investigación constante de información sobre un individuo. Esto lleva a los ciberacosadores a acusar falsamente a sus víctimas, amenazarlas, robar su identidad, dañar su información o los dispositivos que la almacenan, etc. Desde Segurilatam compartimos unas recomendaciones para protegerse del cyberstalking.

4. **Difusión de grabaciones**.

Entre los tipos de violencia cibernética también figuran las grabaciones a personas, sin su consentimiento, para burlarse de ellas, humillarlas, causarlas daños emocionales o reputacionales… En supuestos así, es fundamental denunciar el caso a las Fuerzas y Cuerpos de Seguridad.

5. **‘Grooming’**.

Mediante esta clase de ciberviolencia, los adultos intentan controlar emocionalmente a sus víctimas y chantajearlas con fines sexuales. Se trata de un asunto muy serio. Por ello, es primordial poner en práctica una serie de consejos para no ser víctimas del grooming. Y, ante el menor indicio, poner el asunto en manos de la seguridad pública.
un adulto practica grooming con su ordenador portátilDetrás del ‘grooming’ hay adultos que acosan a menores. Actuaciones que, en muchas ocasiones, no se denuncian.

6. **Extorsión sexual o sextorsión**.

Todo un clásico dentro de los riesgos cibernéticos. La extorsión sexual se fundamenta en las amenazas a un individuo con la publicación de fotos, vídeos o información de carácter sexual si no satisface una cantidad de dinero al extorsionador. Estos tips facilitan prevenir la sextorsión.

7. **Registrar a una persona en un sitio web.**

Otro de los modus operandi de quienes practican violencia cibernética es registrar a sus víctimas en sitios web o aplicaciones sin autorización para que sean ridiculizadas o vejadas. Al tener conocimiento de ello, los perjudicados deben dirigirse a la página web o aplicación, denunciar el caso y solicitar la eliminación de los contenidos.

8. **‘Sexting’**.

El sexting es el envío de textos, imágenes y vídeos de contenido sexual a través de las TIC. Y conlleva una serie de riesgos si los mensajes caen en manos no deseadas. Entre ellos, los ya citados ciberacoso, grooming o sextorsión. Tener en cuenta unos consejos ayuda a prevenir el sexting.

9. **‘Spyware’**.

Algunos delincuentes instalan programas o apps en los dispositivos de sus víctimas. Ello les permite espiarlas y controlarlas. Un primer paso para, una vez conocido el contenido de sus conversaciones o mensajes, chantajearlas o humillarlas. Si un dispositivo relativamente nuevo no funciona con normalidad, es importante comentárselo a un experto en seguridad cibernética para que detecte el origen de la anomalía. Aquí te explicamos qué es el spyware y cómo prevenirlo.

10. **Suplantación de identidad**.

Por último, también es frecuente que se suplante la identidad de una persona para participar en chats u otros entornos digitales donde se compartan comentarios ofensivos. Ello dejaría a la víctima en una situación de desprotección absoluta. Y le obligaría a demostrar que no ha sido ella quien ha publicado los contenidos.

Se practica mediante **aplicaciones de mensajería instantánea**, redes sociales, foros o salas de chat por Internet, correo electrónico o comunidades de juego.

¿**Qué consecuencias tiene** la ciberviolencia para las personas afectadas?

**Daños emocionales y psicológicos**. Y también a los relacionados con la dignidad, intimidad y privacidad. De manera especial, afecta a niñas, mujeres, adolescentes y jóvenes

{{% /note %}}

---

# Soberanía o autonomía tecnológica

{{% note %}}

El concepto de soberanía tecnológica se relaciona con el de la soberanía alimentaria y al igual que este promueve la gestión social de los recursos digitales en pro del desarrollo local, la autonomía y la solidaridad.

La Autonomía Tecnológica es el horizonte en el cual las comunidades toman control de la tecnología para ponerla al servicio de las necesidades e intereses colectivos. La lucha por esta autonomía es una lucha por la colectivización y liberación de los medios tecnológicos, hoy en manos de la dominación capitalista y tecnocrática.


{{% /note %}}

---

{{< slide background-image="https://www.faimaison.net/files/images/chatons-leprette-mai2019/Peha-Banquet-Degooglisons-CC-By.png" background-size="contain" >}}

{{% note %}}

- Tecnología que sirva a las personas y que esté al servicio de defender y ampliar derechos. En vez de responder a un modelo mercantil.
- Enfoque comunitario de uso de herramientas, poder reparar, tener la capacidad de poseer, autoorganizar y autogestionar nuestras herramientas tecnológicas.
- Saber qué usar y auditar todo el código desde el individuo es inabarcable. Debe haber mecanismos de autogestion y poner en comun el elegir que usamos y promovemos. Delegamos en comunidades afines en las que confiamos y lo respaldan.

{{% /note %}}

--- 

## Seguridad poco a poco

* **No hay que asustarse ni agobiarse**.
* Los **cambios** se deben hacer **poco a poco** para no agotarse.
* Rutina de combate e interiorización de la seguridad

![](../memes/wurk_too_hard.png)

{{% note %}}

- Sabiendo que de hoy para mañana no podemos cambiar todas nuestras costumbres, **pensemos en lo práctico**
-  Primero qué cosas puedes hacer de manera individual para **dejar de apoyar macro corporaciones que nos destruyen** y **empezar a usar herramientas que nos protegen un poco más**

[Resistencia Digital]
> La seguridad no debería considerarse una actividad especial, separada del resto de acciones humanas. Tanta es la confusión existente, que se la ha presentado como un momento aparte, cuando es justamente lo contrario: conforme la vayamos desdiferenciando e integrando en las facetas más cotidianas de nuestra existencia, más efectiva resultará. Un recurso típicamente marcial, la rutina de combate, guarda un potencial inesperado al ser aplicado en el contexto de la seguridad en las tecnologías de la información. La idea de la rutina consiste en reproducir condiciones similares a las de la lucha, pero sin riesgo de daños para la persona, en un espacio no hostil para ella. El resultado es la interiorización de estrategias de respuesta que solamente se ganan en circunstancias parecidas a las del combate –el desarrollo de una atención especial sobre los detalles, que es lo que caracteriza al guerrero experimentado–. 
> El objetivo de toda rutina es ganar una inteligencia de la situación, volverla legible a ojos del individuo familiarizado con ella, aunque sea por repetición, con tal de saber moverse en su seno (los simulacros de incendio o terremoto obedecen a esta misma lógica).
> Toda rutina es gradual. Empezar por lo mínimo para ir subiendo el nivel de exigencia gradualmente, conforme se dominen las técnicas básicas. No intentar incorporar herramientas o protocolos de seguridad que no se puedan integrar rápidamente en nuestra experiencia cotidiana, so pena de no saber usarlas o de perder usabilidad.

{{% /note %}}

---

## Seguridad holística

{{% note %}}

- La seguridad es un tema global y transversal. No depende solo de nosotres mismes, sino que requiere de cuidar a todo el grupo
- De aprender y ayudarnos a que todes nuestres amiguis esten comodes con las herramientas que usemos

- Ejemplo de cifrado de correos y si algune no se siente cómode haciéndolo, se acabarán compartiendo por whats app.

{{% /note %}}

---

## Los malos a.k.a modelo de amenaza
+ ¿Qué quieres proteger?
+ Identificación y evaluación de riesgos

---

### Seguridad operacional vs Seguridad intrumental
+ **_Seguridad operacional (OPSEC)_**: se centra en el uso de **procesos y protocolos para aumentar la seguridad**. 
  
+ **_Seguridad instrumental_**: se centra en el uso de **herramientas y aplicaciones para aumentar la seguridad**.

---

## Primeros pasos
	  
+ [**Evaluación de riesgos**](https://ssd.eff.org/es/module/evaluando-tus-riesgos)
+ Análisis de los procesos y vectores de ataque actuales
+ Proteger el **punto más débil**
+ Elabora una **rutina** para empezar poco a poco
+ Inventario de la **mochila de datos** y minimizarla
+ **Separar perfiles**

{{% note %}}

{{% /note %}}

{{% /section %}}

---

{{% section %}}

# Primeros pasos en autodefensa digital

---

## **Contraseñas**:  ![warning.png](../iconos/warning_1686942075028_0.png)

La mayoría de la seguridad depende de las contraseñas, pero todavía existen contraseñas como:
```
passwordf
pepe1997
urss1917
a&n2#yn8
```

---

## **Contraseñas**:  ![warning.png](../iconos/warning_1686942075028_0.png)
Además sigue habiendo filtraciones de contraseñas, exponiendo hasta las contraseñas seguras.

![](https://cdn.le-vpn.com/es/wp-content/uploads/2016/06/databreachinf1200x628v1-es-1.jpg)

---

## **Contraseñas**: ![question.png](../iconos/question_1686942044135_0.png)

+ ¿Cuándo una contraseña es segura?
+ ¿Cómo gestionamos las contraseñas?
+ ¿Qué tipo de autenticación es más segura?

{{% note %}}
- Tipos de autenticación:
	- "Algo que tengo": Token, USB, llave, papel, etc.
	- "Algo que sé": 12345, cumpleaños, frase, Cl4v3, etc.
	- "Algo que soy": huella dactilar, biometría, etc.
- Modelo básico: "Algo que tengo" + "Algo que sé"
- Modelo avanzado: "Algo que tengo" + "Algo que sé" + "Algo que soy"
- Identificación
	- Los actores son quienes dicen ser
- Verificación
	- Comprobar que los actores son quienes dicen ser
	- Que no sea posible que el verificador suplante al verificado
- Autenticación multifactor
	- Two factor Authentication (2FA): Verificación en dos pasos
		- Contraseña
    	- OTP (One-Time Password/Passcode)
{{% /note %}}

---

## **Contraseñas**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png)  

**Seguridad Operacional**
+ **Larga** es mejor que compleja, mínimo 16 carácteres
+ **Cambiarlas** de vez en cuando
+ **Nunca reutilizarlas**
+ Es mejor **no tener que recordarlas**
+ **Usar autenticación multifactor**

{{% note %}}
+ **No poner todos los huevos en la misma cesta**
+ Para recordarla, escribirla a mano o en teclados extranjeros, lo mejor es juntar 6 palabras aleatorias o más usando libros o diccionarios. Quita los acentos, usa sólo 1 género y número gramatical e invéntate una mneotecnia. En inglés le llaman passphrase
+ Más en [ArchWiki: Seguridad](https://wiki.archlinux.org/title/Security_(Espa%C3%B1ol)#Contrase%C3%B1as)
{{% /note %}}

---

## **Contraseñas**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 

**Seguridad Instrumental**
+ [**HaveIBeenPwned**](https://haveibeenpwned.com/)
+ [Mozilla Monitor](https://monitor.mozilla.org/)
+ **Gestores de contraseñas**:
	+ [**KeePassXC**](https://keepassxc.org/)
	+ **[BitWarden](https://bitwarden.com/)**
	+ **[VaultWarden](https://vaultwarden.us/)**

+ Combinar varias **palabras aleatorias** que nos sean fáciles de recordar
+ **Verificación en dos pasos**
	+ [Aegis Authenticator](https://f-droid.org/de/packages/com.beemdevelopment.aegis/)

{{% note %}}
[**HaveIBeenPwned**](https://haveibeenpwned.com/): sitio web para saber si algún servicio que usamos ha sido hackeado
+ **Gestores de contraseñas**:
	+ [**KeePassXC**](https://keepassxc.org/): en local, móvil y con extensión para el navegador. Sincronización con Nextcloud o Syncthing
	+ **[BitWarden](https://bitwarden.com/)**: servicio online de gestión de contraseñas
	+ **[VaultWarden](https://vaultwarden.us/)**: servicio para autoalojar
+ Combinar varias **palabras aleatorias** que nos sean fáciles de recordar
+ Activar la **Verificación en dos pasos** en todos los servicios que lo permitan

{{% /note %}}

---

## **Software**: ![warning.png](../iconos/warning_1686942075028_0.png)
+ Nuevas **vulnerabilidades** y **malware frecuentemente**
+ Aplicaciones de **dudosa procedencia**
+ Sin mantenimiento y **sin actualizaciones**

{{% note %}}
+ Aparecen nuevas vulnerabilidades todos los meses
+ Aparece nuevo **malware frecuentemente**
+ Descargamos e instalamos aplicaciones sin saber qué hacen realmente y cuál es su fuente. Confiar en comunidades grandes activas y en auditorías de seguridad externas
+ Tu **móvil** ya no tiene mantenimiento y **deja de actualizarse**
{{% /note %}}

---

## **Software**: ![question.png](../iconos/question_1686942044135_0.png)
+ ¿Cuándo el **software es seguro**?
+ ¿Riesgos de aplicaciones inseguras? ¿Consecuencias?
+ ¿Código abierto más seguro que código cerrado? ¿GNU/Linux es más seguro que Windows?
+ ¿Protección frente al **acceso remoto**?


{{% note %}}
+ ¿Cuándo se considera que el **software es seguro**?
+ ¿Cuál es el riesgo de que una aplicación no sea segura? ¿Qué consecuencias puede tener?
+ ¿El código libre más seguro que el código privativo? ¿GNU/Linux es más seguro que Windows?
+ ¿Cómo nos protogemos frente al **acceso remoto**?
{{% /note %}}

---

### **Software**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 
**Seguridad Operacional**
+ **Actualizar frecuentemente**
+ Instalar sólo de **fuentes fiables**
+ Reinstalar sistema operativo o aplicaciones cada X tiempo
+ **Auditorías** de seguridad **externas** y **comunidades activas** 
+ **Compartir** preocupaciones, **pensar en colectivo**

{{% note %}}
+ **Actualizar frecuentemente**
+ Instalar sólo de **fuentes fiables**
+ No instalar aplicaciones y olvidarnos de ellas. **Reinstalar sistema operativo cada X tiempo** o revisión de apps. Hacer backups y probar que funcionan.
+ Confiar en las **comunidades activas** y auditorías de seguridad externas
+ **Compartir** las preocupaciones y **pensar en colectivo**
{{% /note %}}

---

### **Software**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 
**Seguridad Instrumental**
+ Anti-Virus y firewalls
  + _Android_: [Hypatia](https://f-droid.org/en/packages/us.spotco.malwarescanner/) (antivirus), [Rethink: DNS + Firewall + VPN](https://f-droid.org/is/packages/com.celzero.bravedns/), [NetGuard](https://f-droid.org/en/packages/eu.faircode.netguard/) (bloqueo de internet por aplicación)
  + _GNU/Linux_: ClamAV (antivirus), UFW (firewall) 
  + _Windows_: WindowsDefender

{{% note %}}
+ Anti-Virus
  + [Hypatia](https://f-droid.org/en/packages/us.spotco.malwarescanner/) para Android
  + ClamAV como antivirus y UFW como firewall para GNU/Linux
  + WindowsDefender es suficientemente bueno y viene por defecto en Windows
{{% /note %}}

---

## **Dispositivos**: ![warning.png](../iconos/warning_1686942075028_0.png)
+ Los móviles anuncian su posición constantemente a las antenas de telefonía.
+ WiFi y Bluetooth también
+ Usado para rastrear e identificar activistas

{{% note %}}
+ Los teléfonos móviles, mediante sus antenas de telefonía anuncian su posición constantemente.
+ WiFi y Bluetooth también pueden (menos común y permite menos rango)
+ Eso se usa para rastrear e identificar a activistas
{{% /note %}}

---

# **Dispositivos**: ![question.png](../iconos/question_1686942044135_0.png)
* ¿Cómo rastrear un teléfono móvil?
* ¿Para qué puede servir conocer las actividades históricas de las personas (participación en eventos o  relaciones personales)?

---

### **Dispositivos**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png)
**Seguridad Operacional**
+ Poner el **modo avión o apagar** cuando no se esté usando
+ Activar opción de **apagado automático de WiFi y Bluetooth** si no está conectado a nada
+ Tener en cuenta qué información ofreces en una protesta

---

### **Dispositivos**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png)
**Seguridad Instrumental**
+ Modo avión
+ Bolsa de Faraday
+ Deja el móvil en casa cuando no quieras que te ubiquen

{{% note %}}
OJO! Una funda de Faraday seguirá guardando cierta información, solo la enviará más tarde.
Lo mismo pasa si lo pones en modo avión, cuando el móvil recupere la conexión a internet enviará metadatos que ha almacenado.
{{% /note %}}

---

## **Bloqueo de móvil**: ![warning.png](../iconos/warning_1686942075028_0.png)
+ Dejas la huella del patrón marcado en la pantalla.
+ Pierdes el móvil por la calle.
+ Te arrestan en una protesta. La policía tiene tu móvil.
+ **¿Qué información podrían obtener?**

---

## **Bloqueo de móvil**: ![question.png](../iconos/question_1686942044135_0.png)
+ Los **teléfonos móviles no fueron diseñados para la privacidad y seguridad** ¿Qué información guardamos en ellos?
+ ¿Qué tiene **mayor valor**: el móvil o los datos que almacenas?

---

## **Bloqueo de móvil**: ![question.png](../iconos/question_1686942044135_0.png)
¿Puede mi adversario ...
  + **observarme desbloquear el teléfono** antes de ganar acceso físico a él?
  + **obligarme o coaccionarme para desbloquear el dispositivo**?
  + **recoger muestras biométricas** (huellas, iris, fotografías) para desbloquear el dispositivo con ellas?
  + contar conocimientos o medios especializados para el **desbloqueo por fuerza bruta** de mi dispositivo?


{{% note %}}
+ ¿Puede mi adversario **observarme desbloquear el teléfono** antes de ganar acceso físico a él?
+ ¿Puede mi adversario **obligarme o coaccionarme para desbloquear el dispositivo**?
+ ¿Mi adversario puede **recoger muestras biométricas** (huellas, iris, fotografías) para desbloquear el dispositivo con ellas?
+ ¿Mi adversario cuenta con conocimientos o medios especializados para el **desbloqueo por fuerza bruta** de mi dispositivo?
{{% /note %}}

---

## **Bloqueo de móvil**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 
**Seguridad Operacional**
+ **Cifrar el móvil**
+ No desbloquearlo si hay cámaras que puedan grabarte
+ Utilizar un **bloqueo seguro**
+ **Compartimentalizar información, separar perfiles**
+ **Conoce al enemigo**

---

## **Bloqueo de móvil**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png)
**Seguridad Instrumental**
+ No **utilizar (o desactivar) el bloqueo biométrico** antes de una protesta
+ **Cifrado** nativo de Android o Apple
+ [Eliminar aplicaciones instaladas de fábrica](https://github.com/0x192/universal-android-debloater)
+ Sistemas operativos enfocados en privacidad: LineageOS, crDroid, DivestOS, ... 
+ Hardenizar Android: [GrapheneOS](https://grapheneos.org/) o [CalyxOS](https://calyxos.org/) 

---

## **Cifrado de ordenador**: ![warning.png](../iconos/warning_1686942075028_0.png)
+ Si se tiene acceso físico al ordenador estás vendide. 
+ Estás de cervezas y te roban la mochila con el ordenador.
+ Si la policía te arresta tendrá acceso físico.

---

## **Cifrado de ordenador**: ![question.png](../iconos/question_1686942044135_0.png)
+ ¿Qué **datos no tenemos cifrados**? ¿Pueden comprometernos a nosotres o a nuestro colectivo?
+ ¿Qué tiene **mayor valor**: el ordenador o los datos que almacenas? ¿Copias de seguridad cifradas?

---

## **Cifrado de ordenador**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png)
**Seguridad Operacional**
+ Mantener el **ordenador apagado** cuando no se usa
+ **Contraseña segura y secreta**

---

## **Cifrado de ordenador**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png)
**Seguridad Instrumental**
+ Linux: [LUKS](https://wiki.archlinux.org/index.php/Dm-crypt) y [más en la ArchWiki](https://wiki.archlinux.org/index.php/Disk_encryption)
+ Windows: [BitLocker](https://docs.microsoft.com/en-us/windows/security/information-protection/bitlocker/bitlocker-overview)
+ Apple: [FileVault](https://support.apple.com/en-us/HT204837)
+ [Cryptomator](https://cryptomator.org/)

{{% note %}}
Cryptomator es un sistema de código abierto, que además de funcionar en sistemas de escritorio también es compatible
con Android e iOS. Con este sistema al igual que con Veracrypt, podrás crear “contenedores” cifrados de
archivos y compartirlos con otras personas o entre dispositivos.

{{% /note %}}

---

## **Conversaciones**: ![warning.png](../iconos/warning_1686942075028_0.png)
+ Para coordinar acciones en grupo es **necesario comunicarse**
+ Siempre hay alguna persona que **habla más de la cuenta**
+ A la policía le gusta saber lo que se dice en esos grupos

---

## **Conversaciones**: ![question.png](../iconos/question_1686942044135_0.png)
+ ¿Qué **temas sensibles** se hablan a través de **grupos abiertos**?
+ ¿Existe un **anillo de confianza** entre las personas del grupo?
+ ¿Qué persona suele **hablar más de la cuenta** de los asustos del colectivo?

---

## **Conversaciones**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) :
**Seguridad Operacional**
+ Cuidado con **quién entra en el grupo**
+ **Borra los mensajes sensibles** o usa chats con autodestrucción
+ Desarrolla un **código secreto para comunicarte** con tus compañeres **en público**
+ Usar **protocolos interoperables y abiertos** para comunicarnos

---

## **Conversaciones**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) :
**Seguridad Operacional**
+ El **punto más débil** no eres tú, puede ser otra persona
+ La seguridad en los grupos es el resultado de la **suma de compromisos individuales** por no exponer información sensible de les demás compañeres
+ Configuración **preferencias de seguridad y privacidad** de las aplicaciones 

---

## **Conversaciones**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) :
**Seguridad Instrumental**

Chats:
+ [Signal](https://www.signal.org/) o [Molly](https://molly.im/)
+ DeltaChat (correo electrónico)
+ Conversations (jabber/xmpp)
+ Element (matrix)
+ Jami (https)
+ [Briar](https://briarproject.org/)
+ aTox

Voz o Videoconferencias
+ [Mumble](https://www.mumble.info/)
+ [Jitsi Meet](https://jitsi.org/jitsi-meet)

{{% note %}}
Briar mensajería cifrada de punto a punto sin necesidad de acceso a Internet. Usa bluetooth, WiFi y Tor
aTox protocolo Tox

{{% /note %}}

---

## **Conversaciones**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) :
**Seguridad Instrumental**

Clientes de Correo electrónico

+ K9-Mail
+ Thunderbird
+ DeltaChat

Proveedores de correo electrónico

+ Riseup
+ Sindominio
+ Disroot
+ Tutanota

---

## **Privacidad y software libre**: ![question.png](../iconos/question_1686942044135_0.png)

+ ¿Piensas que las mega corporaciones tecnológicas conocen tus gustos mejor que tu misme?
+ ¿Eres alergique a los anuncios o a la publicidad? 
+ ¿Pasas mucho tiempo en redes sociales o consumiendo contenido del que luego no te acuerdas?

---

## **Privacidad y software libre**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 

**Repositorios de aplicaciones**
+ F-droid 
+ F-droid Basic
+ Aurora Store

**Teclado**
+ Heliboard
+ Florisboard
+ AnySoftKeyboard

--- 

## **Privacidad y software libre**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 

**Lector Pdfs**
+  Librera

**Reproductor de vídeo**
+ VLC

--- 

## **Privacidad y software libre**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 

**Sincronización de Archivos**

- Nextcloud
- Syncthing

**Contactos/Calendario**
- Davx5
- Etar (calendario)

---

## **Privacidad y software libre**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 

**Entretenimiento Audiovisual**
- RiMusic
- Tubular/NewPipe
- AntennaPod
- Jellyfin

**Redes sociales libres**
- Mastodon
- Agendas de Gancio
- Peertube
- Pixelfed

---

## **Privacidad y software libre**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 

**Frontend alternativo de redes privativas**
- UntrackMe

**Mapas**

+ Open Street Maps
+ Organic Maps

---

## **Privacidad y software libre**: ![lightbulb.png](../iconos/lightbulb_1686942086742_0.png) 

**Tor y VPN**

- Tor Browser
- Orbot
- Riseup VPN
- Calyx VPN
- Wireguard
- Mullvad
- OpenVPN

{{% /section %}}

---

{{% section %}}

# Seguridad analógica

---

## Dinámica 1

---

## Dinámica 2

{{% /section %}}

---

{{% section %}}

# Listado de cuidados de seguridad

{{% /section %}}

---

{{% section %}}


# Cierre y Conclusiones
{{% note %}}
+ La cuestión de seguridad no hay que abordarlo desde lo individual sino desde lo colectivo 
+ Rutinas donde ir introduciendo nuevos elementos poco a poco

{{% /note %}}

{{% /section %}}

---

{{% section %}}

# Referencias

---

## Fanzines

- [MUCHO MÓVIL, POCA DIVERSIÓN. Hacklab Logout.](https://sindominio.net/logout/fanzine/)
- [Security culture. Building relationships of trust and care](https://climatedefenseproject.org/wp-content/uploads/2023/07/7.5.23_Digital_Security-Culture-Zine.pdf)
- [Colección de Zines del Colectivo Disonancia](https://colectivodisonancia.net/zines/)

---

## Libros y textos

- [Introducción a la Cultura de la Seguridad. Ediciones Extáticas. 2021.](https://edicionesextaticas.noblogs.org/post/textos-y-traducciones/introduccion-a-la-cultura-de-la-seguridad/)
- Resistencia Digital - Manual de seguridad operacional e instrumental para smartphones. Críptica.2019.
- La era del capitalismo de la vigilancia. Shoshana Zuboff. 2020.

---

## Guías y manuales

- [Colectivo disonancia](https://colectivodisonancia.net/)
- [Colectivo 406](https://406.neocities.org/c/guias/)
- [Holistic Security Manual](https://holistic-security.tacticaltech.org/index.html)
- [Guía de Defensa Digital para Organizaciones Sociales](https://lalibre.net/wp-content/uploads/2022/09/Guia-de-proteccion-digital.pdf)
- [Manual de autodefensa en la era de la digitalización forzada o Resistencia al capitalismo de vigilancia](https://codeberg.org/PrivacyFirst/Data_Protection/issues)

---

## Infografías

---

{{< slide background-image="https://web.karisma.org.co/wp-content/uploads/2023/06/INFOGRAFIA-WEB-GUIA-100-1-scaled.jpg" background-size="contain" >}}

{{% /section %}}