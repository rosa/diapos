+++
title = "TeknoKasa"
outputs = ["Reveal"]
[logo]
src = "teknokasa/teknokasa_logo_full.png"

+++

{{% section %}}

# TeknoKasa

La oficina de okupación digital

---

Fomentemos una comunidad de experimentación y reflexión tecnopolítica

---

Desarrollemos herramientas de intervención política

---

Pongamos la tecnología al servicio de los movimientos sociales

---

Reflexionemos colectivamente sobre cultura libre o seguridad digital y física

--- 

Produzcamos comunes digitales que fomenten la soberanía tecnológica

---

Talleres reproducibles:
- <a href="taller_web" target="_blank">**Taller de edición y generación de web estáticas con Hugo**</a>

- <a href="cultura_seguridad" target="_blank">**Taller de cultura de la seguridad**</a>

{{% /section %}}